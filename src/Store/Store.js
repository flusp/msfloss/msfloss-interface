import { createStore } from 'redux';
import { Reducers } from './Reducers/Reducers.js';

export const Store = createStore(Reducers);
