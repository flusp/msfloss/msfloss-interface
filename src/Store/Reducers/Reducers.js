import { allProjects } from './ProjectsReducer';
import { combineReducers } from 'redux';

export const Reducers = combineReducers({
  projects: allProjects
});
