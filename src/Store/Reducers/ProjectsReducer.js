import C from "../Actions/ActionsConstants.js"

const initialState = {
  id: null,
  name: "",
  gitUrl: "",
  membersTotal: 0,
  commitsTotal: 0,
  lastUpdated: new Date()
}

export const allProjects = (state=[], action) => {
  switch(action.type) {
    case C.ADD_PROJECT:
      const existsInState = state.some(project => project.id === action.payload.id)
      return (existsInState) ?
         state :
         [
           ...state,
           {...initialState,
              id: action.payload.id,
              name: action.payload.name,
              gitUrl: action.payload.gitUrl,
              lastUpdated: action.payload.lastUpdated}
         ]
   case C.ADD_STATISTIC:
     return (
       state.map(project => {
         if (project.id === action.payload.id) {
           return {...project,
                    id: action.payload.id,
                    membersTotal: action.payload.membersTotal,
                    commitsTotal: action.payload.commitsTotal}
         }
         return project
       }))
    case C.REMOVE_PROJECT:
      return state.filter(project => project.id !== action.payload)
    default:
      return state
  }
}
