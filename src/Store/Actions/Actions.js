import C from "./ActionsConstants.js"

export function addProject(id, name, gitUrl, lastUpdated) {
  return {
      type: C.ADD_PROJECT,
      payload: {id: id, name: name, gitUrl: gitUrl, lastUpdated: lastUpdated}
  }
}

export function addStatistic(id, membersTotal, commitsTotal) {
  return {
      type: C.ADD_STATISTIC,
      payload: {id: id, membersTotal: membersTotal, commitsTotal: commitsTotal}
  }
}

export function removeProject(id) {
    return {
        type: C.REMOVE_PROJECT,
        payload: id
    }

}
