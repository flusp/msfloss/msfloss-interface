import axios from 'axios'

export const commitService = axios.create({
  baseURL: "https://staging-msfloss-api.herokuapp.com/api/v1/",
  headers: {
    "Accept": "application/json",
    post: {
      "Content-Type": "application/json"
    }
  }
})

export function getProjects(handleSuccess, handleError) {
  commitService.get("/projects")
  .then(function (data) {
    handleSuccess(data)
  })
  .catch(function (error) {
    handleError(error)
  })
}

export function getRandomProjects(number, handleSuccess, handleError) {
  commitService.get("/projects?random=true&limit=" + number)
  .then(function (data) {
    handleSuccess(data)
  })
  .catch(function (error) {
    handleError(error)
  })
}

export function postProject(name, url, handleSuccess, handleError) {
  commitService.post("/projects/", '{"project":{"project_name":"' + name + '", "git_url":"' + url + '"}}')
  .then(function (data) {
    handleSuccess(data)
  })
  .catch(function (error) {
    handleError(error)
  })
}

export function getProject(projectId, handleSuccess, handleError) {
  commitService.get("/projects/" + projectId)
  .then(function (data) {
    handleSuccess(data)
  })
  .catch(function (error) {
    handleError(error)
  })
}

export function getCommits(projectId, handleSuccess, handleError) {
  commitService.get("/projects/" + projectId + "/commits")
  .then(function (data) {
    handleSuccess(data)
  })
  .catch(function (error) {
    handleError(error)
  })
}

export function getStatistics(projectId, handleSuccess, handleError) {
  commitService.get("/projects/" + projectId + "/commits/statistics")
  .then(function (data) {
    handleSuccess(data)
  })
  .catch(function (error) {
    handleError(error)
  })
}

export function getDiffs(projectId, handleSuccess, handleError) {
  commitService.get("/projects/" + projectId + "/commits/diffs")
  .then(function (data) {
    handleSuccess(data)
  })
  .catch(function (error) {
    handleError(error)
  })
}
