import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Description.css';

class Description extends Component {
  render() {
    return (
      <div className="description">
          <p className="text">
            <span>Motivation</span>
            Free/Libre/Open Source Software (FLOSS) is an umbrella term for software in which users are free to inspect, use, change, and distribute it [1].  For over two decades, investigations have sought to describe better the practices surrounding a successful FLOSS project.<br/>

            In 1999, Raymond published “The Cathedral and the Bazaar”, defining the development model o free software as a Bazaar [2].  In this seminal essay, the author coined the following statement: “Given enough eyeballs, all bugs are shallow”, which became known as the “Linus Law”. A crucial aspect of FLOSS is how structured are their ways of deciding the north of the project. Many channels of communication are used, naming Mail lists, Version control Repositories, IRC channels, bug trackers, Websites, and Wikis [3].<br/>

            In this context, the StarFLOSS project wants to observe how FLOSS communities work. By using some of their channels to gather information and cohesively present them, we want to create a tool that allows any person interested to understand the inner workings of any given community in a fun and easy way.<br/>

            <br/>
            <span>How it Works?</span>
            Donec nec tincidunt nisl. Donec vitae dui justo. Duis a arcu nec lectus lobortis luctus. Phasellus sit amet augue cursus, mattis nunc vel, euismod libero. Maecenas gravida ultricies malesuada. Donec posuere nisl et ullamcorper porttitor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis sit amet faucibus augue, sed mattis nulla. Duis cursus vitae orci in tincidunt.
          </p>
        </div>
    )
  }
}

Description.propTypes = {
}

export default Description
