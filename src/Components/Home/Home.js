import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addProject, addStatistic } from '../../Store/Actions/Actions.js';
import PropTypes from 'prop-types';
import SkyBanner from './SkyBanner/SkyBanner.js';
import Description from './Description/Description.js';
import Ranking from './Ranking/Ranking.js';
import Resume from '../Commons/Resume/Resume.js';
import Navbar from '../Navbar/Navbar.js';
import * as commitService from '../../Services/CommitService.js'

import './Home.css';

import observatory from '../../Assets/observatory.svg'

class Home extends Component {
  constructor(props) {
    super(props)
    this.loadProjects = this.loadProjects.bind(this);
    this.loadStatistics = this.loadStatistics.bind(this);
  }

  componentDidMount() {
    this.loadProjects()
  }

  loadProjects() {
    commitService.getProjects(
      function(data){
        data = Object.entries(data.data)
        for (var i = 0; i < data.length; i++) {
          var d = data[i][1]
          this.props.addProject(d.id, d.project_name, d.git_url, d.updated_at)
          this.loadStatistics(d.id)
        }
      }.bind(this),
      function(error) {
      }.bind(this)
    )
  }

  loadStatistics(id) {
    commitService.getStatistics(id,
      function(data) {
        var d = data.data
        this.props.addStatistic(id, d.distinct_members, d.total_commits)
      }.bind(this),
      function(error) {
      }
    )
  }

  render() {
    return (
      <div className="home">
        <div className="image-div">
          <img className="image" src={observatory}/>
        </div>
        <SkyBanner/>
        <Description/>
        <Ranking/>
      </div>
    )
  }
}

const mapStateToProps = store => ({
  projects: store.projects
});
const mapDispatchToProps = dispatch =>
  bindActionCreators({ addProject, addStatistic }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home)
