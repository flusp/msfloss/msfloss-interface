import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import Star from '../../Commons/Star/Star.js';
import './SkyBanner.css';
import observatory from '../../../Assets/observatory.svg'
import * as commitService from '../../../Services/CommitService.js'

class SkyBanner extends Component {
  constructor(props) {
    super(props)
    this.state = {
      random: []
    }
    this.loadRandom = this.loadRandom.bind(this);
  }

  componentDidMount() {
    this.loadRandom()
  }

  loadRandom() {
    commitService.getRandomProjects(6,
      function(data){
        data = Object.entries(data.data)
        this.setState({random: [data[0][1], data[1][1], data[2][1], data[3][1], data[4][1], data[5][1]]})
      }.bind(this),
      function(error) {
      }.bind(this)
    )
  }

  eachStar(project, index) {
    var stat = this.props.projects[project.id - 1]
    if (stat === undefined) {
      return
    }
    return(
      <Link className="link" to={"/dashboard/" + project.id}>
      <Star key={index} divId={"banner-star" + index} divClasses="banner-star" commits={stat.commitsTotal} members={stat.membersTotal}/>
      </Link>
    )
  }

  render() {
    return (
      <div className="skybanner">
        <div className="main">
          <h1 className="title">StarFLOSS</h1>
          <h2 className="subtitle">An observatory for Free Software</h2>
        </div>
        {this.state.random.map((project, index) => this.eachStar(project, index))}
      </div>
    )
  }
}

SkyBanner.propTypes = {
  projects: PropTypes.array
}

const mapStateToProps = store => ({
  projects: store.projects
})
export default connect(mapStateToProps)(SkyBanner)
