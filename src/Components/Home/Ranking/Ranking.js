import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Resume from '../../Commons/Resume/Resume.js';
import * as commitService from '../../../Services/CommitService.js'

import './Ranking.css';

class Ranking extends Component {
  constructor(props) {
    super(props)
    this.eachProject = this.eachProject.bind(this);
  }

  eachProject(project) {
    return (
      <Resume name={project.name}
              key={project.id}
              index={project.id}
              members={project.membersTotal}
              commits={project.commitsTotal}
              update={new Date(project.lastUpdated).toString()}/>
    )
  }


  render() {
    return (
      <div className="ranking">
        <h2>Recently Updated
        </h2>
        <div className="resume-list">
          {this.props.projects.map(this.eachProject)}
        </div>
        <div className="more">Add project</div>
      </div>
    )
  }
}

Ranking.propTypes = {
  projects: PropTypes.array
}

const mapStateToProps = store => ({
  projects: store.projects
})
export default connect(mapStateToProps)(Ranking)
