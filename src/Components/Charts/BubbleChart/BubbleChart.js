import React, { Component } from 'react'
import PropTypes from 'prop-types'
import * as d3 from 'd3v4'
import * as d3Color from 'd3-scale-chromatic'
import './BubbleChart.css'
import * as commitService from '../../../Services/CommitService.js'


class BubbleChart extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dalt: false
    }
    commitService.getDiffs(this.props.projectId,
      function(data) {
        var parseDate = d3.timeParse("%Y-%m-%dT%H:%M:%S%Z")
        var data = data.data
        var dates = [];
        for (var i = 0; i < data.length; i++) {
          for (var j = 0; j < data[i].commits.length; j++) {
            data[i].commits[j].date = parseDate(data[i].commits[j].date)
            data[i].commits[j]["order"] = data[i].commits.length - j - 1
            dates.push(data[i].commits[j].date)
          }
          // data[i].commits = data[i].commits.sort(function(a, b){ return a.date - b.date });
        }
        this.setState({data: data, shownData: data, dates: dates})
      }.bind(this),
      function(error) {
        console.log(error)
      }
    )

    this.props.dispatch.on("heatmap.bubble", function(params){
      if (params.reset) {
        this.reset()
        return
      }

      var pd = []
      for (var i = 0; i < this.state.data.length; i++) {
        pd[i] = {name: this.state.data[i].name, commits: []}
      }

      var dates = []
      for (i = 0; i < params.dates.length; i++) {
        var hour = params.dates[i].hour.split("hours")[1]
        var day = params.dates[i].week.split("weekDays")[1]

        for (var j = 0; j < this.state.data.length; j++) {
          pd[j].commits = pd[j].commits.concat(this.state.data[j].commits.filter(function(d) {
            dates.push(d.date);
            return ((d.date.getHours() == hour) && (d.date.getDay() == day))
          }))
        }
      }

      this.setState({shownData: pd, dates: dates})
    }.bind(this))

    this.props.dispatch.on("bubble.bubble", function(){
      var classes = d3.select(d3.event.path[1]).attr("class").replace('active', '').replace('active', '')
      if (d3.select(d3.event.path[1]).classed("active")) {
        d3.select(d3.event.path[1]).attr("class", classes)
      } else {
        d3.select(d3.event.path[1]).attr("class", classes + " active")
      }
    })

    this.props.dispatch.on("members.bubble", function(params){
      if (params.reset) {
        this.reset()
        return
      }
      var pd = this.state.data.filter(function(d) {return d.name === params.data})
      var dates = []
      for (var i = 0; i < pd.length; i++) {
        for (var j = 0; j < pd[i].commits.length; j++) {
          dates.push(pd[i].commits[j].date)
        }
      }
      this.setState({shownData: pd, dates: dates})
    }.bind(this))

    this.reset = this.reset.bind(this)
  }

  componentDidUpdate() {
    d3.select(".bubble-chart svg").remove()
    this.drawBubbleChart(this.state.shownData, this.state.dates)
  }

  reset() {
    var dates = []
    for (var i = 0; i < this.state.data.length; i++) {
      for (var j = 0; j < this.state.data[i].commits.length; j++) {
        dates.push(this.state.data[i].commits[j].date)
      }
    }
    this.setState({shownData: this.state.data, dates: dates})
    return
  }

  drawBubbleChart(data, dates) {
    const margin = {top: 20, left: 30, down: 20, right: 30}
    const height = 300
    const width = 500
    const viewHeight = height + margin.top + margin.down
    const viewWidth = width + margin.left + margin.right


    const xScale = d3.scaleTime()
            .domain(d3.extent(dates))
            .range([0, width])
    const xAxis = d3.axisBottom(xScale)

    var yScale = d3.scaleLinear()
                .domain([0, d3.max(data, function(d){return d3.max(d.commits, function (d1) {return d1.order})})])
                .range([height,0]);
    const yAxis = d3.axisLeft(yScale)

    const maxlines = d3.max(data, function(d){ return d3.max(d.commits, function(d1){ return d1.added_lines + d1.deleted_lines})})
    var rScale = d3.scaleLinear()
                .domain([0, maxlines])
                .range([2, 40]);

    const maxAdded = d3.max(data, function(d){ return d3.max(d.commits, function(d1){ return d1.added_lines - d1.deleted_lines})})
    const maxDeleted = d3.max(data, function(d){ return d3.max(d.commits, function(d1){ return d1.deleted_lines - d1.added_lines})})
    const maxAltered = d3.max([maxAdded, maxDeleted])
    const blueRed = d3.scaleLinear().domain([0, 0.5, 1]).range(['#d73027', '#6baed6', '#1a9850']).interpolate(d3.interpolateHcl);
    const greenRed = d3.scaleLinear().domain([0, 1]).range(['#d73027', '#1a9850']).interpolate(d3.interpolateHcl);
    // const red = d3.scaleLinear().domain([0, maxDeleted]).range('#6baed6', '#d73027']).interpolate(d3.interpolateHcl);
// green: '#1a9850'
    var mouseover = function(d) {
      d3.selectAll(".commits")
        .transition()
        .duration(200)
        .style("opacity", 0.1)
      d3.select(this.parentNode)
        .transition()
        .duration(200)
        .style("opacity", 1)
      d3.select(".bubble-chart .tooltip")
        .transition()
        .duration(200)
        .style("opacity","1")
      d3.select(".bubble-chart .tooltip").html("Commiter: " + d.author_name + "<br/>Day: " + d.date.toString().split("GMT")[0])
    }

    var mouseleave = function(d) {
      d3.selectAll(".commits")
        .transition()
        .duration(200)
        .style("opacity", 0.8)
      d3.select(".bubble-chart .tooltip")
        .transition()
        .duration(200)
        .style("opacity","0")
    }

    d3.select(".bubble-chart").append("div").attr("class", "tooltip").style("opacity","0").style("position","absolute");
    const svg = d3.select(".bubble-chart").append("svg").attr("viewBox", "0 0 " + viewWidth + " " + viewHeight).attr("id", this.props.divId)
    const chartGroup = svg.append("g").attr("transform","translate(" + margin.left + "," + margin.top + ")")

    var commits = chartGroup.selectAll("g")
      .data(data)
      .enter()
        .append("g")
        .attr("class", function(d,i){ return "commits commits" + i })
        .attr("fill-opacity", ".8")

    var circles = commits.selectAll("g")
      .data(function(d){ return d.commits })
      .enter()
        .append("circle")
        .attr("class",function(d,i){ return "circle circle" + i })
        .attr("fill", function(d) { return blueRed(d.added_lines/(d.added_lines + d.deleted_lines))})
        .attr("cx",function(d){ return xScale(d.date) })
        .attr("cy",function(d){ return yScale(d.order) })
        .attr("r",function(d){ return rScale(d.added_lines + d.deleted_lines) })
        .on("mouseover", mouseover)
        .on("mouseleave", mouseleave)
        .on("click", function(d) {
          this.props.dispatch.call("bubble", this, d);
        }.bind(this))

    chartGroup.append("g")
      .attr("class","axis x")
      .attr("transform","translate(0," + 300 + ")")
      .call(xAxis)

    chartGroup.append("g")
      .attr("class","axis y")
      .call(yAxis);
  }

  render() {
    return (
      <div className="bubble-chart">
      </div>
    )
  }
}

BubbleChart.propTypes = {
  projectId: PropTypes.string,
  divId: PropTypes.string,
  dispatch: PropTypes.object
}

export default BubbleChart
