import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Heatmap from '../Charts/Heatmap/Heatmap.js';
import BubbleChart from '../Charts/BubbleChart/BubbleChart.js';
import RelationChart from '../Charts/RelationChart/RelationChart.js';
import Star from '../Commons/Star/Star.js';
import * as commitService from '../../Services/CommitService.js'

import './Infograph.css';

import data from '../../Assets/Data/commits.js'

class Infograph extends Component {
  constructor() {
    super()

    this.state = {
      data: data
    }

    // commitService.postProject("Linux", "https://github.com/d3/d3",
    //   function(data) {
    //     console.log(data)
    //   },
    //   function(error) {
    //     console.log(error)
    //   }
    // )

    // commitService.getProjects(
    //   function(data) {
    //     console.log(data)
    //   },
    //   function(error) {
    //     // console.log(error)
    //   }
    // )
    //
    // commitService.getStatistics(3,
    //   function(data) {
    //     console.log(data)
    //   },
    //   function(error) {
    //     // console.log(error)
    //   }
    // )
  }

  render() {
    return (
      <div className="infograph">
        This will be the infograph, but for now it's just a mishmash of charts
        <RelationChart divId="id2"/>
      </div>
    )
  }
}

export default Infograph
