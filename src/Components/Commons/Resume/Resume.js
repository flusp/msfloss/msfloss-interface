import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import Star from '../Star/Star.js';
import './Resume.css';

class Resume extends Component {
  render() {
    return (
        <div className="resume">
          <Star members={this.props.members}
                commits={this.props.commits}
                divId={"resume" + this.props.index}
                divClasses={"resume-star"}/>
          <div className="info">
            <p className="title"><span>{this.props.name}</span></p>
            <p><span>Members: </span>{this.props.members}</p>
            <p><span>Last Update: </span>{this.props.update}</p>
            <Link className="link" to={"/dashboard/" + this.props.index}>See more</Link>
          </div>
        </div>
    )
  }
}

Resume.propTypes = {
  index: PropTypes.number,
  name: PropTypes.string,
  members: PropTypes.number,
  commits: PropTypes.number,
  activity: PropTypes.string,
  update: PropTypes.string,
}

export default Resume
