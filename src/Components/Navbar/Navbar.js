import React, { Component } from 'react';
import { Link } from "react-router-dom";
import PropTypes from 'prop-types';
import './Navbar.css';

class Navbar extends Component {
  render() {
    return (
      <div className="navbar">
        <Link className="link" to="/">Home</Link>
        <Link className="link" to="/dashboard/3">Dashboard</Link>
        <Link className="link" to="/infograph">Infograph</Link>
      </div>
    )
  }
}

export default Navbar
