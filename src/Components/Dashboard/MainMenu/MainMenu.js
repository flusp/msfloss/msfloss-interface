import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Heatmap from '../../Charts/Heatmap/Heatmap.js';
import BubbleChart from '../../Charts/BubbleChart/BubbleChart.js';
import './MainMenu.css';

class MainMenu extends Component {
  render() {
    return (
      <div className="mainmenu">
        <div className="chart">
          <h3>Time of day - Heatmap</h3>
          <Heatmap projectId={this.props.dataId} divId="dashboard-id" dispatch={this.props.dispatch}/>
        </div>
        <div className="chart">
          <h3>Commits over time - Bubble chart</h3>
          <BubbleChart projectId={this.props.dataId} divId="dashboard-id2" dispatch={this.props.dispatch}/>
        </div>
      </div>
    )
  }
}

MainMenu.propTypes = {
  dataId: PropTypes.string,
  dispatch: PropTypes.object
}

export default MainMenu
